# VEGAS JS CORE OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.11] - 2021-05-15
### Changed
- Update rollup and babel

## [1.0.10] - 2021-05-15
### Changed
- Refactoring with ES6 
- Use the mocha config file

## [1.0.9] - 2021-05-14
### Changed
- Refactoring and change the vegas-js-core version : 1.0.32

## [1.0.5] - 2018-11-08
## [1.0.4] - 2018-11-08
### Changed
- Fix the ValueObject class implementation and JSDoc

## [1.0.3] - 2018-11-03
### Changed
- Refactoring and change the vegas-js-core version : 1.0.5 

## [1.0.0] - 2018-10-28
### Added
- First version

