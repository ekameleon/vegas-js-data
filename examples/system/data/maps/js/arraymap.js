/* globals vegas_data */
"use strict" ;

window.onload = function()
{
    if( !vegas_data )
    {
        throw new Error("The VEGAS JS - DATA library is not found.") ;
    }
    
    const data = vegas_data ;

    let map = new data.maps.ArrayMap() ;

    map.set("key1", "value1") ;
    map.set("key2", "value2") ;
    map.set("key3", "value3") ;

    console.log ("map : " + map) ;
    console.log ("map length: " + map.length) ;

    console.log ("------ iterator") ;

    let iterator = map.iterator() ;
    while (iterator.hasNext())
    {
        console.log(iterator.next() + " : " + iterator.key()) ;
    }

    console.log ("------") ;

    console.log( 'values : ' + map.values()) ;
    console.log( "map.has('key2') : " + map.has('key2')) ;
    console.log( "map.get('key2') : " + map.get('key2') ) ;
    console.log( "map.indexOfKey('key2') : " + map.indexOfKey('key2')) ;

    map.delete( 'key2' ) ;

    console.log ("------ forEach") ;

    map.forEach( function( value , key , map )
    {
        console.log( "map[" + key + "] = " + value + " in " + map );
    });
};