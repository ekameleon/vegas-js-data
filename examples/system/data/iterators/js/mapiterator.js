/* globals vegas */
"use strict" ;

window.onload = function()
{
    if( !vegas_data )
    {
        throw new Error("The VEGAS JS - DATA library is not found.") ;
    }
    
    const data = vegas_data ;
    
    let map = new data.maps.ArrayMap() ;
    
    map.set("key1", "value1") ;
    map.set("key2", "value2") ;
    map.set("key3", "value3") ;

    console.log( '> ' + map ) ;

    let it = map.iterator() ;
    while( it.hasNext() )
    {
        console.log (it.next() + " : " + it.key()) ;
        if( it.key() === "key2" )
        {
            it.delete() ;
        }
    }
    
    console.log( '> ' + map ) ;
}