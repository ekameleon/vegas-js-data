/* globals vegas */
"use strict" ;

window.onload = function()
{
    if( !vegas_data )
    {
        throw new Error("The VEGAS JS - DATA library is not found.") ;
    }
    
    const data = vegas_data ;

    let ArrayIterator = data.iterators.ArrayIterator ;

    let ar = ["item1", "item2", "item3", "item4"] ;
    
    let it = new ArrayIterator(ar) ;

    while (it.hasNext())
    {
        console.log(it.next()) ;
    }
    
    console.log("--- it reset") ;

    it.reset() ;

    while (it.hasNext())
    {
        console.log(it.next() + " : " + it.key()) ;
    }
    
    console.log("--- it seek 2") ;
    
    console.log( ar.length ) ;
    it.seek(2) ;
    while (it.hasNext())
    {
        console.log(it.next()) ;
        it.delete() ;
    }
    console.log( ar.length ) ;
    
    console.log("---") ;

}