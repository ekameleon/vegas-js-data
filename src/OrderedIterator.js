/*jshint unused: false*/
"use strict" ;

import Iterator from './Iterator'

/**
 * Defines an iterator that operates over an ordered collection. This iterator allows both forward and reverse iteration through the collection.
 * @name OrderedIterator
 * @extends system.data.Iterator
 * @interface
 * @memberof system.data
 */
class OrderedIterator extends Iterator
{
    /**
     * Checks to see if there is a previous element that can be iterated to.
     * @memberof system.data.OrderedIterator
     * @function
     * @instance
     */
    hasPrevious = () => {}

    /**
     * Returns the previous element in the collection.
     * @return the previous element in the collection.
     * @memberof system.data.OrderedIterator
     * @function
     * @instance
     */
    previous = () => {}

}

export default OrderedIterator ;