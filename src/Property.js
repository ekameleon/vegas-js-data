"use strict" ;

import notEmpty from 'vegas-js-core/src/strings/notEmpty';

/**
 * This interface should be implemented by any properties definition object.
 * @name Property
 * @class
 * @memberof system.data
 * @see system.data.Attribute
 * @see system.data.Method
 * @see system.process.Cache
 */
class Property
{
    /**
     * Creates a new Property instance.
     * @constructor
     * @param {string|null} [name=null] The name of the method.
     */
    constructor( name = null )
    {
        /**
         * The name of the method.
         * @name name
         * @memberof system.data.Method
         * @type {string}
         */
        this.name = notEmpty( name ) ? name : null ;
    }

    /**
     * Returns the string representation of this instance.
     * @return the string representation of this instance.
     * @name toString
     * @memberof system.data.Method
     * @function
     * @instance
     */
    toString = () => "[" + this.constructor.name + "]" ;
}

export default Property ;