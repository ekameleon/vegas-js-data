"use strict" ;

import notEmpty from 'vegas-js-core/src/strings/notEmpty'

import Property from './Property'

/**
 * Determinate a basic <b>method</b> definition.
 * @summary Determinate a basic <b>method</b> definition.
 * @name Method
 * @class
 * @memberof system.data
 * @extends Property
 * @param {string|null} [name=null] The name of the method.
 * @param {array} [args=null] The optional array of arguments of the method.
 * @see system.data.Attribute
 * @see system.process.Cache
 */
class Method extends Property
{
    /**
     * Creates a new Method instance.
     * @constructor
     * @param {string|null} [name=null] The name of the method.
     * @param {array} [args=null] The optional array of arguments of the method.
     */
    constructor( name = null , args = null )
    {
        super( name ) ;

        /**
         * The optional array of arguments of the method.
         * @name args
         * @memberof system.data.Method
         * @type {array}
         */
        this.args = (args instanceof Array) ? args : null ;
    }
}

export default Method ;