/*jshint laxbreak : true*/
/*jshint unused   : false*/
"use strict"

/**
 * Defines the methods that objects that participate in a validation operation.
 * @name Validator
 * @interface
 * @memberof system.data
 */
class Validator
{
    /**
     * Indicates if the validator supports the passed-in value.
     * @param {*} value - The value to evaluate.
     * @return {boolean} {b<code>true</code> if the specific value is valid.
     * @memberof system.data.Validator
     * @function
     */
    supports = value => {}

    /**
     * Evaluates the specified value and throw an <code>Error</code> if the value is not valid.
     * @param {*} value - The value to evaluate.
     * @memberof system.data.Validator
     * @function
     * @throws <code>Error</code> if the value is not valid.
     */
    validate = value => {}
}

export default Validator ;