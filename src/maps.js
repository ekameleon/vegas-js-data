"use strict" ;

import ArrayMap from './maps/ArrayMap'

/**
 * This package contains all {@link system.data.KeyValuePair|KeyValuePair} extended implementations : {@link system.data.maps.ArrayMap|ArrayMap}, etc.
 * @summary This package contains all {@link system.data.KeyValuePair|KeyValuePair} extended implementations : {@link system.data.maps.ArrayMap|ArrayMap}, etc.
 * @namespace system.data.maps
 * @memberof system.data
 */
const maps =
{
    ArrayMap
};

export default maps ;