"use strict" ;

import Property from './Property'

/**
 * Determinate a basic <b>attribute</b> definition.
 * @summary Determinate a basic <b>attribute</b> definition.
 * @name Attribute
 * @class
 * @memberof system.data
 * @implements system.data.Property
 * @param {string} name The name of the attribute.
 * @param {*} value The value of the attribute.
 * @see system.data.Method
 * @see system.process.Cache
 */
class Attribute extends Property
{
    constructor( name = null , value = undefined )
    {
        super( name ) ;

        /**
         * The value of the attribute.
         * @name name
         * @memberof system.data.Attribute
         * @type {*}
         */
        this.value = value ;
    }
}

export default Attribute ;