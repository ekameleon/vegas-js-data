"use strict" ;

/**
 * This interface defines a common structure for <strong>identifiable</strong> classes (has an <code>id<code> property).
 * @name Identifiable
 * @interface
 * @memberof system.data
 */
class Identifiable
{
    /**
     * Creates a new Identifiable instance.
     * @constructor
     * @param {*} id - The unique identifier of this object.
     */
    constructor( id = null )
    {
        Object.defineProperties( this ,
        {
            /**
             * Indicates the unique identifier value of this object.
             * @name id
             * @memberof system.data.Identifiable
             * @instance
             */
            id : { value : id , writable : true }
        });
    }
}

export default Identifiable;