"use strict" ;

import Identifiable from './Identifiable'

/**
 * Defines a value object. The value object are use for example in the models or to exchange datas between a client and a server.
 * @summary Defines a basic value object.
 * @name ValueObject
 * @class
 * @memberof system.data
 * @extends system.data.Identifiable
 */
class ValueObject extends Identifiable
{
    /**
     * @constructor
     * @param {Object|Null} [init=null] - A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    constructor( init = null )
    {
        super() ;
        if( init )
        {
            this.set( init ) ;
        }
    }
    
    /**
     * A utility function for implementing the <code>toString()</code> method in custom classes. Overriding the <code>toString()</code> method is recommended, but not required.
     * @name formatToString
     * @memberof system.data.ValueObject
     * @instance
     * @function
     * @param {_constructorName|{value}} className - The class name to passed-in the string expression.
     * @param {...string} [rest] - rest The properties of the class and the properties that you add in your custom class.
     * @return {string} The formatted string representation of the object.
     * @example
     * class Thing extends ValueObject
     * {
     *     constructor( name )
     *     {
     *         this.name = name;
     *     }
     *
     *     toString()
     *     {
     *         return this.formatToString( this.constructor.name , "name" );
     *     }
     * }
     */
    formatToString = ( className = null , ...rest ) =>
    {
        if( className === null )
        {
            if( !(this._constructorName) )
            {
                Object.defineProperties( this , { _constructorName : { value : this.constructor.name } } );
            }
            className = this._constructorName ;
        }
        let ar = [ className ] ;
        let len = rest.length ;
        for( let i = 0; i < len ; ++i )
        {
            if( rest[i] in this )
            {
                ar.push( rest[i] + ":" + this[rest[i]] ) ;
            }
        }
        return "[" + ar.join(' ') + "]" ;
    }
    
    /**
     * Sets the members of the object to the specified values.
     * @param {*} init - The generic object to initialize the object.
     * @return {ValueObject} ttThe current object reference.
     * @memberof system.data.ValueObject
     * @instance
     * @function
     */
    set = ( init = null ) =>
    {
        if( init )
        {
            for( let prop in init )
            {
                if( prop in this )
                {
                    this[prop] = init[prop];
                }
            }
        }
        return this ;
    }
    
    /**
     * Returns the string representation of this object.
     * @return {string} the string representation of this object.
     * @memberof system.data.ValueObject
     * @instance
     * @function
     */
    toString = () => this.formatToString( null ) ;
}

export default ValueObject ;