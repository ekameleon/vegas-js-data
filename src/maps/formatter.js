"use strict" ;

import MapFormatter from './MapFormatter'

/**
 * The {@link system.data.maps.MapFormatter|MapFormatter} singleton.
 * @name formatter
 * @instance
 * @const
 * @type system.data.maps.MapFormatter
 * @memberof system.data.maps
 */
const formatter = new MapFormatter() ;

export default formatter ;