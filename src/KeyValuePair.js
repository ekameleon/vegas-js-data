/*jshint unused: false*/
"use strict" ;

/**
 * An object that maps keys to values. A map cannot contain duplicate keys. Each key can map to at most one value.
 * <p><b>Note:</b> This class replace the old <code>system.data.Map</code> interface in the VEGAS framework. Today in Javascript the {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map|Map} class is a standard global <b>ECMAScript</b> definition.</p>
 * @summary An object that maps keys to values.
 * @name KeyValuePair
 * @class
 * @memberof system.data
 */
class KeyValuePair
{
    /**
     * Returns the number of key-value mappings in this map.
     * @name length
     * @memberof system.data.KeyValuePair
     * @instance
     * @type {number}
     * @readonly
     */
    get length() { return 0; }

    /**
     * Removes all mappings from this map (optional operation).
     * @name clear
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    clear = () => {}

    /**
     * Returns a shallow copy of the map.
     * @return a shallow copy of the map.
     * @name clone
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    clone = () => new KeyValuePair();

    /**
     * Copies all of the mappings from the specified map to this map (optional operation).
     * @param {system.data.KeyValuePair} map - The map to fill the current map.
     * @name setAll
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    copyFrom = map => {};

    /**
     * The <code>delete()</code> method removes the specified element from a KeyValuePair object.
     * @param {*} key - The key of the entry to remove.
     * @name delete
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    delete = key => {};

    /**
     * The forEach() method executes a provided function once per each key/value pair in the KeyValuePair object, in insertion order.
     * @param callback Function to execute for each element.
     * @param thisArg Value to use as this when executing callback.
     * @name forEach
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    forEach = ( callback, thisArg = null ) => {}

    /**
     * Returns the value to which this map maps the specified key.
     * @param {*} key - The key of the entry to retrieve in the collection.
     * @name get
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    get = key => null;

    /**
     * Returns <code>true</code> if this map contains a mapping for the specified key.
     * @param {*} key - The key of the entry to retrieve in the collection.
     * @return <code>true</code> if this map contains a mapping for the specified key.
     * @name has
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    has = key => false;

    /**
     * Returns <code>true</code> if this map maps one or more keys to the specified value.
     * @return <code>true</code> if this map maps one or more keys to the specified value.
     * @name hasValue
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    hasValue = value => false;

    /**
     * Returns <code>true</code> if this map contains no key-value mappings.
     * @return <code>true</code> if this map contains no key-value mappings.
     * @name isEmpty
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    isEmpty = () => true;

    /**
     * Returns the values iterator of this map.
     * @return the values iterator of this map.
     * @name iterator
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    iterator = () => null;

    /**
     * Returns the keys iterator of this map.
     * @return the keys iterator of this map.
     * @name keyIterator
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    keyIterator = () => null;

    /**
     * Returns an <code>Array</code> of all the keys in the map.
     * @return an <code>Array</code> representation of all the keys register in this collection.
     * @name keys
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    keys = () => null;

    /**
     * Associates the specified value with the specified key in this map (optional operation).
     * @param {*} key - The key of the element to add to the Map object.
     * @param {*} value - The value of the element to add to the Map object.
     * @name set
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    set = ( key, value ) => {};

    /**
     * Returns the string representation of this instance.
     * @return the string representation of this instance.
     * @name toString
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    toString = () => '[' + this.constructor.name + ']';

    /**
     * Returns an array of all the values in the map.
     * @return an <code>Array</code> representation of all the values register in this collection.
     * @name values
     * @memberof system.data.KeyValuePair
     * @instance
     * @function
     */
    values = () => null;
}

export default KeyValuePair ;