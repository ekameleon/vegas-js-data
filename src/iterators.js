"use strict" ;

import ArrayIterator from './iterators/ArrayIterator'
import MapIterator   from './iterators/MapIterator'

/**
 * This package contains all {@link system.data.Iterator|Iterator} implementations : {@link system.data.iterators.ArrayIterator|ArrayIterator}, {@link system.data.iterators.MapIterator|MapIterator}, etc.
 * @summary This package contains all {@link system.data.Iterator|Iterator} implementations : {@link system.data.iterators.ArrayIterator|ArrayIterator}, {@link system.data.iterators.MapIterator|MapIterator}, etc.
 * @namespace system.data.iterators
 * @memberof system.data
 */
const iterators =
{
    ArrayIterator,
    MapIterator
};

export default iterators ;