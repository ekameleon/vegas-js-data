/*jshint laxbreak : true*/
/*jshint unused   : false*/
"use strict" ;

import Validator from './Validator'

/**
 * Indicates if the specific <code>target</code> is a {@link system.data.Validator|Validator} object.
 * @name isValidator
 * @memberof system.data
 * @function
 * @param {object} target - The object to evaluate.
 * @return <code>true</code> if the object is a {@link system.data.Validator|Validator}.
 */
const isValidator = target => target
                           && (
                               target instanceof Validator
                            || (
                                (('supports' in target) && target.supports instanceof Function)
                             && (('validate' in target) && target.validate instanceof Function)
                               ));

export default isValidator  ;