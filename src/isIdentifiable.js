"use strict" ;

import Identifiable from './Identifiable'

/**
 * Indicates if the specific <code>target</code> is an {@link system.data.Identifiable|Identifiable} object.
 * @name isIdentifiable
 * @memberof system.data
 * @function
 * @param {object} target - The object to evaluate.
 * @return A <code>true</code> value if the object is an {@link system.data.Identifiable|Identifiable} object.
 */
const isIdentifiable = target => target && ( ( target instanceof Identifiable ) || ( target.hasOwnProperty("id") ) ) ;

export default isIdentifiable ;