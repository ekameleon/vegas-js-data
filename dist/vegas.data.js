/**
 * A specialized set of functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc. - version: 1.0.11 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.vegas_data = factory());
}(this, (function () { 'use strict';

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);

      if (enumerableOnly) {
        symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      }

      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  var Identifiable =
  function Identifiable() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    _classCallCheck(this, Identifiable);
    Object.defineProperties(this, {
      id: {
        value: id,
        writable: true
      }
    });
  };

  var isIdentifiable = function isIdentifiable(target) {
    return target && (target instanceof Identifiable || target.hasOwnProperty("id"));
  };

  var Iterator = function Iterator() {
    var _this = this;
    _classCallCheck(this, Iterator);
    _defineProperty(this, "delete", function () {});
    _defineProperty(this, "hasNext", function () {
      return false;
    });
    _defineProperty(this, "key", function () {
      return null;
    });
    _defineProperty(this, "next", function () {
      return null;
    });
    _defineProperty(this, "reset", function () {});
    _defineProperty(this, "seek", function (position) {});
    _defineProperty(this, "toString", function () {
      return '[' + _this.constructor.name + ']';
    });
  };

  var isIterator = function isIterator(target) {
    if (target) {
      return target instanceof Iterator ||
      Boolean(target['delete']) && target["delete"] instanceof Function && Boolean(target['hasNext']) && target.hasNext instanceof Function && Boolean(target['key']) && target.key instanceof Function && Boolean(target['next']) && target.next instanceof Function && Boolean(target['reset']) && target.reset instanceof Function && Boolean(target['seek']) && target.seek instanceof Function
      ;
    }
    return false;
  };

  var OrderedIterator = function (_Iterator) {
    _inherits(OrderedIterator, _Iterator);
    var _super = _createSuper(OrderedIterator);
    function OrderedIterator() {
      var _this;
      _classCallCheck(this, OrderedIterator);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      _defineProperty(_assertThisInitialized(_this), "hasPrevious", function () {});
      _defineProperty(_assertThisInitialized(_this), "previous", function () {});
      return _this;
    }
    return OrderedIterator;
  }(Iterator);

  var isOrderedIterator = function isOrderedIterator(target) {
    if (target) {
      return target instanceof OrderedIterator || 'hasNext' in target && target.hasNext instanceof Function && 'hasPrevious' in target && target.hasPrevious instanceof Function && 'key' in target && target.key instanceof Function && 'next' in target && target.next instanceof Function && 'previous' in target && target.previous instanceof Function && 'remove' in target && target.remove instanceof Function && 'reset' in target && target.reset instanceof Function && 'seek' in target && target.seek instanceof Function;
    }
    return false;
  };

  var Validator = function Validator() {
    _classCallCheck(this, Validator);
    _defineProperty(this, "supports", function (value) {});
    _defineProperty(this, "validate", function (value) {});
  };

  var isValidator = function isValidator(target) {
    return target && (target instanceof Validator || 'supports' in target && target.supports instanceof Function && 'validate' in target && target.validate instanceof Function);
  };

  var KeyValuePair = function () {
    function KeyValuePair() {
      var _this = this;
      _classCallCheck(this, KeyValuePair);
      _defineProperty(this, "clear", function () {});
      _defineProperty(this, "clone", function () {
        return new KeyValuePair();
      });
      _defineProperty(this, "copyFrom", function (map) {});
      _defineProperty(this, "delete", function (key) {});
      _defineProperty(this, "forEach", function (callback) {
      });
      _defineProperty(this, "get", function (key) {
        return null;
      });
      _defineProperty(this, "has", function (key) {
        return false;
      });
      _defineProperty(this, "hasValue", function (value) {
        return false;
      });
      _defineProperty(this, "isEmpty", function () {
        return true;
      });
      _defineProperty(this, "iterator", function () {
        return null;
      });
      _defineProperty(this, "keyIterator", function () {
        return null;
      });
      _defineProperty(this, "keys", function () {
        return null;
      });
      _defineProperty(this, "set", function (key, value) {});
      _defineProperty(this, "toString", function () {
        return '[' + _this.constructor.name + ']';
      });
      _defineProperty(this, "values", function () {
        return null;
      });
    }
    _createClass(KeyValuePair, [{
      key: "length",
      get:
      function get() {
        return 0;
      }
    }]);
    return KeyValuePair;
  }();

  const notEmpty = value => (typeof value === 'string' || value instanceof String) && value !== '';

  var Property =
  function Property() {
    var _this = this;
    var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    _classCallCheck(this, Property);
    _defineProperty(this, "toString", function () {
      return "[" + _this.constructor.name + "]";
    });
    this.name = notEmpty(name) ? name : null;
  }
  ;

  var Attribute = function (_Property) {
    _inherits(Attribute, _Property);
    var _super = _createSuper(Attribute);
    function Attribute() {
      var _this;
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
      _classCallCheck(this, Attribute);
      _this = _super.call(this, name);
      _this.value = value;
      return _this;
    }
    return Attribute;
  }(Property);

  var Method = function (_Property) {
    _inherits(Method, _Property);
    var _super = _createSuper(Method);
    function Method() {
      var _this;
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      _classCallCheck(this, Method);
      _this = _super.call(this, name);
      _this.args = args instanceof Array ? args : null;
      return _this;
    }
    return Method;
  }(Property);

  var ValueObject = function (_Identifiable) {
    _inherits(ValueObject, _Identifiable);
    var _super = _createSuper(ValueObject);
    function ValueObject() {
      var _this;
      var _init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      _classCallCheck(this, ValueObject);
      _this = _super.call(this);
      _defineProperty(_assertThisInitialized(_this), "formatToString", function () {
        var className = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        if (className === null) {
          if (!_this._constructorName) {
            Object.defineProperties(_assertThisInitialized(_this), {
              _constructorName: {
                value: _this.constructor.name
              }
            });
          }
          className = _this._constructorName;
        }
        var ar = [className];
        for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          rest[_key - 1] = arguments[_key];
        }
        var len = rest.length;
        for (var i = 0; i < len; ++i) {
          if (rest[i] in _assertThisInitialized(_this)) {
            ar.push(rest[i] + ":" + _this[rest[i]]);
          }
        }
        return "[" + ar.join(' ') + "]";
      });
      _defineProperty(_assertThisInitialized(_this), "set", function () {
        var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        if (init) {
          for (var prop in init) {
            if (prop in _assertThisInitialized(_this)) {
              _this[prop] = init[prop];
            }
          }
        }
        return _assertThisInitialized(_this);
      });
      _defineProperty(_assertThisInitialized(_this), "toString", function () {
        return _this.formatToString(null);
      });
      if (_init) {
        _this.set(_init);
      }
      return _this;
    }
    return ValueObject;
  }(Identifiable);

  function ArrayIterator(array) {
    if (!(array instanceof Array)) {
      throw new ReferenceError(this + " constructor failed, the passed-in Array argument not must be 'null'.");
    }
    Object.defineProperties(this, {
      _a: {
        value: array,
        writable: true
      },
      _k: {
        value: -1,
        writable: true
      }
    });
  }
  ArrayIterator.prototype = Object.create(Iterator.prototype, {
    constructor: {
      value: ArrayIterator
    },
    "delete": {
      value: function value() {
        return this._a.splice(this._k--, 1)[0];
      }
    },
    hasNext: {
      value: function value() {
        return this._k < this._a.length - 1;
      }
    },
    key: {
      value: function value() {
        return this._k;
      }
    },
    next: {
      value: function value() {
        return this._a[++this._k];
      }
    },
    reset: {
      value: function value() {
        this._k = -1;
      }
    },
    seek: {
      value: function value(position) {
        position = Math.max(Math.min(position - 1, this._a.length), -1);
        this._k = isNaN(position) ? -1 : position;
      }
    }
  });

  function MapIterator(map) {
    if (map && map instanceof KeyValuePair) {
      Object.defineProperties(this, {
        _m: {
          value: map,
          writable: true
        },
        _i: {
          value: new ArrayIterator(map.keys()),
          writable: true
        },
        _k: {
          value: null,
          writable: true
        }
      });
    } else {
      throw new ReferenceError(this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.");
    }
  }
  MapIterator.prototype = Object.create(Iterator.prototype, {
    constructor: {
      writable: true,
      value: MapIterator
    },
    "delete": {
      value: function value() {
        this._i["delete"]();
        return this._m["delete"](this._k);
      }
    },
    hasNext: {
      value: function value() {
        return this._i.hasNext();
      }
    },
    key: {
      value: function value() {
        return this._k;
      }
    },
    next: {
      value: function value() {
        this._k = this._i.next();
        return this._m.get(this._k);
      }
    },
    reset: {
      value: function value() {
        this._i.reset();
      }
    },
    seek: {
      value: function value(position) {
        throw new Error("This Iterator does not support the seek() method.");
      }
    }
  });

  var iterators = {
    ArrayIterator: ArrayIterator,
    MapIterator: MapIterator
  };

  function MapFormatter() {}
  MapFormatter.prototype = Object.create(Object.prototype, {
    constructor: {
      writable: true,
      value: MapFormatter
    },
    format: {
      value: function value(_value) {
        if (_value instanceof KeyValuePair) {
          var r = "{";
          var keys = _value.keys();
          var len = keys.length;
          if (len > 0) {
            var values = _value.values();
            for (var i = 0; i < len; i++) {
              r += keys[i] + ':' + values[i];
              if (i < len - 1) {
                r += ",";
              }
            }
          }
          r += "}";
          return r;
        } else {
          return "{}";
        }
      }
    }
  });

  var formatter = new MapFormatter();

  function MapEntry(key, value) {
    Object.defineProperties(this, {
      key: {
        value: key,
        writable: true
      },
      value: {
        value: value,
        writable: true
      }
    });
  }
  MapEntry.prototype = Object.create(Object.prototype, {
    constructor: {
      value: MapEntry
    },
    clone: {
      value: function value() {
        return new MapEntry(this.key, this.value);
      }
    },
    toString: {
      value: function value() {
        return "[MapEntry key:" + this.key + " value:" + this.value + "]";
      }
    }
  });

  function ArrayMap() {
    var keys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    Object.defineProperties(this, {
      _keys: {
        value: [],
        writable: true
      },
      _values: {
        value: [],
        writable: true
      }
    });
    if (keys === null || values === null) {
      this._keys = [];
      this._values = [];
    } else {
      var b = keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length;
      this._keys = b ? [].concat(keys) : [];
      this._values = b ? [].concat(values) : [];
    }
  }
  ArrayMap.prototype = Object.create(KeyValuePair.prototype, {
    constructor: {
      writable: true,
      value: ArrayMap
    },
    length: {
      get: function get() {
        return this._keys.length;
      }
    },
    clear: {
      value: function value() {
        this._keys = [];
        this._values = [];
      }
    },
    clone: {
      value: function value() {
        return new ArrayMap(this._keys, this._values);
      }
    },
    copyFrom: {
      value: function value(map) {
        if (!map || !(map instanceof KeyValuePair)) {
          return;
        }
        var keys = map.keys();
        var values = map.values();
        var l = keys.length;
        for (var i = 0; i < l; i = i - -1) {
          this.set(keys[i], values[i]);
        }
      }
    },
    "delete": {
      value: function value(key) {
        var v = null;
        var i = this.indexOfKey(key);
        if (i > -1) {
          v = this._values[i];
          this._keys.splice(i, 1);
          this._values.splice(i, 1);
        }
        return v;
      }
    },
    forEach: {
      value: function value(callback) {
        var thisArg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        if (typeof callback !== "function") {
          throw new TypeError(callback + ' is not a function');
        }
        var l = this._keys.length;
        for (var i = 0; i < l; i++) {
          callback.call(thisArg, this._values[i], this._keys[i], this);
        }
      }
    },
    get: {
      value: function value(key) {
        return this._values[this.indexOfKey(key)];
      }
    },
    getKeyAt: {
      value: function value(index) {
        return this._keys[index];
      }
    },
    getValueAt: {
      value: function value(index
      ) {
        return this._values[index];
      }
    },
    has: {
      value: function value(key) {
        return this.indexOfKey(key) > -1;
      }
    },
    hasValue: {
      value: function value(_value) {
        return this.indexOfValue(_value) > -1;
      }
    },
    indexOfKey: {
      value: function value(key) {
        var l = this._keys.length;
        while (--l > -1) {
          if (this._keys[l] === key) {
            return l;
          }
        }
        return -1;
      }
    },
    indexOfValue: {
      value: function value(_value2) {
        var l = this._values.length;
        while (--l > -1) {
          if (this._values[l] === _value2) {
            return l;
          }
        }
        return -1;
      }
    },
    isEmpty: {
      value: function value() {
        return this._keys.length === 0;
      }
    },
    iterator: {
      value: function value() {
        return new MapIterator(this);
      }
    },
    keyIterator: {
      value: function value()
      {
        return new ArrayIterator(this._keys);
      }
    },
    keys: {
      value: function value() {
        return this._keys.concat();
      }
    },
    set: {
      value: function value(key, _value3) {
        var r = null;
        var i = this.indexOfKey(key);
        if (i < 0) {
          this._keys.push(key);
          this._values.push(_value3);
        } else {
          r = this._values[i];
          this._values[i] = _value3;
        }
        return r;
      }
    },
    setKeyAt: {
      value: function value(index, key) {
        if (index >= this._keys.length) {
          throw new RangeError("ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.");
        }
        if (this.containsKey(key)) {
          return null;
        }
        var k = this._keys[index];
        if (k === undefined) {
          return null;
        }
        var v = this._values[index];
        this._keys[index] = key;
        return new MapEntry(k, v);
      }
    },
    setValueAt: {
      value: function value(index, _value4) {
        if (index >= this._keys.length) {
          throw new RangeError("ArrayMap.setValueAt(" + index + ") failed with an index out of the range.");
        }
        var v = this._values[index];
        if (v === undefined) {
          return null;
        }
        var k = this._keys[index];
        this._values[index] = _value4;
        return new MapEntry(k, v);
      }
    },
    toString: {
      value: function value() {
        return formatter.format(this);
      }
    },
    values: {
      value: function value() {
        return this._values.concat();
      }
    }
  });

  var maps = {
    ArrayMap: ArrayMap
  };

  /**
   * The {@link system.data} library provides a framework unified for representing and manipulating <b>collections</b>, enabling them to be manipulated independently of the details of their representation.
   * <p>It reduces programming effort while increasing performance. It enables interoperability among unrelated APIs, reduces effort in designing and learning new APIs, and fosters software reuse.</p>
   * <p>The framework is based on a serie of interfaces. It includes implementations of these interfaces and algorithms to manipulate them.</p></br>
   * <p>An <strong>abstract data type</strong> (<b>ADT</b>) is a model for a certain class of data structures that have similar behavior; or for certain data types of one or more programming languages that have similar semantics. The collections framework is a unified architecture for representing and manipulating collections, allowing them to be manipulated independently of the details of their representation. It reduces programming effort while increasing performance.</p>
   * <p>Originaly the {@link system.data} collection framework is loosely inspired on the <b>JAVA Collections Framework</b> and the <b>Jakarta Collections Framework</b> but with the new ES6 standard we change the basic implementation of the new <b>VEGAS</b> framework in the JS version of the library.</p>
   * <p>This framework is inspired on interfaces to defines the different types of collections : * Map * Bag * Collections * Iterator * Set * Queue & Stack... </p>
   * @summary The {@link system.data} library provides a framework unified for representing and manipulating <b>collections</b>.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.data
   * @memberof system
   */
  var data = {
    isIdentifiable: isIdentifiable,
    isIterator: isIterator,
    isOrderedIterator: isOrderedIterator,
    isValidator: isValidator,
    Identifiable: Identifiable,
    Iterator: Iterator,
    KeyValuePair: KeyValuePair,
    OrderedIterator: OrderedIterator,
    Property: Property,
    Validator: Validator,
    Attribute: Attribute,
    Method: Method,
    ValueObject: ValueObject,
    iterators: iterators,
    maps: maps
  };

  var skip = false;
  function sayHello(name = '', version = '', link = '') {
    if (skip) {
      return;
    }
    try {
      if (navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        const args = [`\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`, 'background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #000000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'background: #ff0000; padding:5px 0;'];
        window.console.log.apply(console, args);
      } else if (window.console) {
        window.console.log(`${name} ${version} - ${link}`);
      }
    } catch (error) {
    }
  }
  function skipHello() {
    skip = true;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-data'
    },
    description: {
      enumerable: true,
      value: 'A specialized set of functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.'
    },
    version: {
      enumerable: true,
      value: '1.0.11'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-data'
    }
  });
  var bundle = _objectSpread2({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.data.js.map
